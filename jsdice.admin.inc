<?php

/**
 * @file
 * administration form
 */


/**
 * Define the settings form
 */
function jsdice_settings() {
  // global settings
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global'),
    '#description' => t('jsDice settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['global']['jsdice_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for the jsDice page'),
    '#default_value' => variable_get('jsdice_title', 'jsDice'),
    '#description' => 'Uses this as the title of the page. Defaults to "jsDice".',
  );
  $form['global']['jsdice_drupal_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a Drupal friendly CSS'),
    '#default_value' => variable_get('jsdice_drupal_css', 0),
    '#description' => 'WARNING: If you have cache features turned on, this may not work until the cache is refreshed or cleared.',
  );

  return system_settings_form($form);
}
