<?php

/**
 * @file
 * display the dice roller interface
 */


/**
 * Implementation of jsdice_page().
 */
function jsdice_page() {
  drupal_set_title(variable_get('jsdice_title', 'jsDice'));
  return '
    <div id="jsdice-page">
      <div id="main">
        <h1>
          <a href="http://www.jsdice.com/roller/" title="Click to download the original open source javascript dice roller"><em>js</em>Dice
            - Dungeons & Dragons Dice Roller</a>
        </h1>
        <noscript>
          <div>
            <h2>Warning: Javascript required!</h2>
            <p>
              <em>js</em>Dice is a simple yet flexible dice roller for
              roleplaying games. It is fully written in client-side javascript,
              which means that you can use it offline. For more information
              please use a javascript-enabled browser and click on the
              <em>help</em> link.
            </p>
            <p>
              <em>js</em>Dice is open source, distributed under the FreeBSD
              license, included in the beginning of this .html file (view
              source to see it).
            </p>
          </div>
        </noscript>
        <div id="jsdice" class="hidden">
          <div id="results"></div>
          <div id="inp">
            <small>Dice to roll: [<a href="javascript:doHelp();">Help</a>]</small><br/>
            <input id="inp_text" type="text" onkeypress="return inp_keydown(event);"/><button
            id="inp_go" onclick="go();">Go!</button>
          </div>
          <div id="pre">
            <button id="pre_d4" onclick="doRoll(\'1d4\');">d4</button>
            <button id="pre_d6" onclick="doRoll(\'1d6\');">d6</button>
            <button id="pre_d8" onclick="doRoll(\'1d8\');">d8</button>
            <button id="pre_d10" onclick="doRoll(\'1d10\');">d10</button>
            <button id="pre_d12" onclick="doRoll(\'1d12\');">d12</button>
            <button id="pre_d20" onclick="doRoll(\'1d20\');">d20</button>
            <button id="pre_d100" onclick="doRoll(\'1d100\');">d100</button>
            <button id="pre_clear" onclick="doClear();">Clear</button>
          </div>
        </div>
        <div id="jsdice-contact">
          For offline use <a href="http://www.jsdice.com/roller/jsdice.html"
          rel="nofollow">download jsdice.html</a>. You can send me a message
          at wiz jsdice com. Feel free to contact me for comments, suggestions,
          bug reports, rants or whatever as long as you do not spam. If you
          like <em>js</em>Dice send me a (scanned) postcard!
        </div>
        <h6>&copy; 2009 jsdice.com - v0.11 - best viewed in Mozilla Firefox</h6>
      </div>
    </div>';
}
